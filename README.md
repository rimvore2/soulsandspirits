# Souls and Spirits

This submod adds two new fatal vore goals and their enablers to RimVore-2

## Soul Vore

The Metaphysical Disassembler allow your predators to trap the souls of your enemies into their bodies and break it down into pure vital energy greatly strengthening their bodies and minds. Strike Fear into the souls of your enemies and increase your gurgling efficiency!

## Distillation vore

The Endostillic Converter allow your predators to work as fully enclosed and (possibly) multi-functional distillery, and mush these prisoners down into booze!


Requirements:  
	- RimVore 2 (Overstuffed)